import Company from "../mongoDB/CompanyModel.js";


const allCompanies = async function showAllCompanies(req, res) {

    const companies = await Company.find({}).sort({ _id: -1 });

    res.status(200).json(companies);
};


export {
    allCompanies
}