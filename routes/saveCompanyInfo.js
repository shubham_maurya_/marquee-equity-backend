import Company from "../mongoDB/CompanyModel.js";

// Validator to return any error if it exists
import {validationResult } from "express-validator";


const saveCompanyData = function saveCompanyData(req, res) {

    console.log(req.body.companies);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    // No error data is ok! Process to Insert data

    //const company = new Company({name: req.body.name, cin: req.body.cin});

    Company.insertMany(req.body.companies).then(() => {
        // Data is saved to database show success msg to user
        console.log("Company Info Saved!");
        return res.status(200).json({"msg" : "Company Info Saved!"})
    }).catch(errors => {
        console.error('Unable to save to database');
        // Respond with error! Unable to save data
        return res.status(400).json({"error" : "Can not save to database"})
    });

};

export {
    saveCompanyData
}