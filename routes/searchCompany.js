import axios from "axios";

const searchCompany = function (req, res) {
    axios({
        method: 'post',
        url: "https://www.zaubacorp.com/custom-search",
        data: {
            search : req.query.company,
            filter: 'company'
        },
        header:{
            "accept": "*/*",
            "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Origin" : "https://www.zaubacorp.com",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest"
        }
    }).then(result => {
        res.status(200).send(result.data);
    }).catch(error => {
        console.error(error);
        res.status(400);
    });
};


export {
    searchCompany
}