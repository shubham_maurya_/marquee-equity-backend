/*
*   Connect to mongodb with mongoose framework
*
 */

import mongoose from "mongoose";


mongoose.connect('mongodb://localhost/marquee_db', function(){
    console.log('mongodb connected!')
});

export {
    mongoose
}