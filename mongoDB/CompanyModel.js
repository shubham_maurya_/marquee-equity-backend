/*
*  Defining Company model to perform db operations
*
 */


import {mongoose} from "./connection.js";

export default mongoose.model('Company',
    {
        name: {type: String, required: true},
        cin: {type: String, required: true}
    }
);
