## Backend for Technical Test Marquee Equity

Backend based on ExpressJs
To run the backend simply install dependency and run "npm run serve"

Directory Info

/mongoDB - Contains all the database related file
    /connection - establish connection via mongoose orm
    /CompanyModel - mongoose model for Company
    
/routes - these are controller/functions for each endpoint

/index.js - entry point for application, also contains routes