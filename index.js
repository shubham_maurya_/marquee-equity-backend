// Express server to serve the api

import express from "express";
import bodyParser from "body-parser";

import cors from "cors";
// Validator to check posted data to api

import { body, validationResult } from "express-validator";

//Routes import
import {saveCompanyData} from "./routes/saveCompanyInfo.js";
import {allCompanies} from "./routes/allCompanies.js";
import {searchCompany} from "./routes/searchCompany.js";

const app = express();

//enable Cross Origin Request
app.use(cors());

// parse application/json
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const port = 3000;

app.get('/', allCompanies);

app.post('/save', saveCompanyData);


app.get('/custom-search',  searchCompany);


app.listen(port, () => {
    console.log(`Marquee Backend listening at http://localhost:${port}`)
});